package uk.co.adilparvez.f_droidiconmod;


import de.robv.android.xposed.IXposedHookInitPackageResources;
import de.robv.android.xposed.IXposedHookZygoteInit;
import de.robv.android.xposed.callbacks.XC_InitPackageResources;

public class XposedModule implements IXposedHookZygoteInit, IXposedHookInitPackageResources {

    @Override
    public void initZygote(StartupParam startupParam) throws Throwable {
        FDroidMod.initZygote(startupParam);
    }

    @Override
    public void handleInitPackageResources(XC_InitPackageResources.InitPackageResourcesParam initPackageResourcesParam) throws Throwable {
        FDroidMod.handleInitPackageResources(initPackageResourcesParam);
    }

}
