package uk.co.adilparvez.f_droidiconmod;


import android.content.res.XModuleResources;

import de.robv.android.xposed.IXposedHookZygoteInit;
import de.robv.android.xposed.callbacks.XC_InitPackageResources;

public class FDroidMod {

    private static String MODULE_PATH;

    public static void initZygote(IXposedHookZygoteInit.StartupParam startupParam) throws Throwable {
        MODULE_PATH = startupParam.modulePath;
    }

    public static void handleInitPackageResources(XC_InitPackageResources.InitPackageResourcesParam initPackageResourcesParam) throws Throwable {
        if (!initPackageResourcesParam.packageName.equals("org.fdroid.fdroid")) {
            return;
        }

        XModuleResources moduleResources = XModuleResources.createInstance(MODULE_PATH, initPackageResourcesParam.res);
        initPackageResourcesParam.res.setReplacement("org.fdroid.fdroid:drawable/ic_launcher", moduleResources.fwd(R.drawable.ic_launcher));
    }

}
